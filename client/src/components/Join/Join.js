import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, TextField, withStyles } from '@material-ui/core';

import './Join.css';

const CssTextField = withStyles({
  root: {
    '& label': {
      color: 'white',
      fontSize: '10pt',
    },
    '& label.Mui-focused': {
      color: 'white',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'white',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'white',
      },
      '&:hover fieldset': {
        borderColor: 'white',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'white',
      },
    },
  },
})(TextField);

const Join = () => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');

  return (
    <div className='join__outerContainer'>
      <div className='join__innerContainer'>
        <h1 className='join__heading'>Join</h1>
        <CssTextField
          required
          className='join__input'
          variant='outlined'
          label='Name'
          type='text'
          inputProps={{ style: { color: 'white', fontSize: '12pt' } }}
          onChange={(event) => setName(event.target.value)}
        />
        <CssTextField
          required
          className='join__input'
          variant='outlined'
          label='Room'
          type='text'
          inputProps={{ style: { color: 'white', fontSize: '12pt' } }}
          onChange={(event) => setRoom(event.target.value)}
        />
        <Link
          onClick={(event) => (!name || !room ? event.preventDefault() : null)}
          to={`/chat?name=${name}&room=${room}`}>
          <Button className='join__signIn' fullWidth>
            Sign In
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Join;
